TODO
====

- [ ] Use https://gitlab.torproject.org/cve/torblock

Usage
=====

Connection
----------

To both connect your laptop to the Raspberry Pi (without going through it's
censored Wi-Fi) and give Internet connectivity to the Raspberry Pi, you can
either:

- Connect to the same local Wi-Fi network

- Connect your laptop to the Raspberry Pi with an Ethernet cable and the
  Raspberry Pi to the Internet with USB tethering

Local Wi-Fi (simpler but more hardware)
---------------------------------------

1. Plug a Wi-Fi adapter in the Raspberry Pi.

1. Use `nmcli device` to verify that the device is called `wlan-usb-uplink`.

   To change the device name to `wlan-usb-uplink`, update the MAC address in
   `/etc/systemd/network/03-wlan-usb-uplink.link` and reboot.

1. Use `nmtui` to connect `wlan-usb-uplink` to the local Wi-Fi network.

1. To find out the local IP address of the Raspberry Pi, you can try to connect
   first to the Wi-Fi that the Raspberry Pi is emitting.

       ssh pi@192.168.3.1

       ssh mora.wlan

1. Connect to the Raspberry Pi from the shared local Wi-Fi network.

       ssh pi@192.168.2.1

       ssh mora.lan

Ethernet + USB tethering (works without another Wi-Fi network)
--------------------------------------------------------------

From your laptop:

1. Connect to the Raspberry Pi with an Ethernet cable.

   Make sure that the Raspberry Pi is answering DHCP requests.

   /!\ In April 2024, I didn't manage to connect using an Ethernet cable
   despite probably using the same setup than in Brazil. LEDs were not blinking
   and tools like mii-tool returned "no link".

2. Connect to the Raspberry Pi with SSH:

       ssh pi@192.168.2.1

       ssh mora.lan

3. Plug the Raspberry Pi to your phone and activate USB tethering to give
   Internet connectivity to the Raspberry Pi.

   Connecting the phone to a Wi-Fi network works and is more reliable than
   using mobile data.

   The Raspberry Pi should also forward connections from your laptop to
   the Internet via the USB tethering.

Simulating different networks
-----------------------------

### Manually

- Block Tor and default bridges:

  ferm /etc/ferm/ferm.conf --def "BLOCK_TOR=1"

  Maybe update the list of fallback dirs in /root/tor/src/app/.

- Unblock Tor:

  ferm /etc/ferm/ferm.conf --def "BLOCK_TOR=0"

### Preconfigured

See /usr/local/sbin/config.

For example:

- Blocking nothing

      # config home

- Blocking some websites

      # config turkey

- Blocking Tor and Telegram

      # config thailand

- Captive portal

      # config train

To emulate a disconnection:

    # nmcli device disconnect wlan-raspi-ap

Wait until Tails disconnects.

    # nmcli device connect wlan-raspi-ap

Bridges
-------

obfs4 104.168.175.42:443 F369D235DFAE703DBD6DDF7FEF4CB945B06CE152 cert=GrDMVEjlYGznY5Qb6v36q4ILPYmocekLSZsZhi1zLVC6p740xJzs5Y02lVbe6H1m/Vw3Yg iat-mode=0
obfs4 185.220.101.152:12346 EE04D908C44E01D4CCF1699A9FA5BFF9EF21C41F cert=p9L6+25s8bnfkye1ZxFeAE4mAGY7DH4Gaj7dxngIIzP9BtqrHHwZXdjMK0RVIQ34C7aqZw iat-mode=2
obfs4 73.237.243.83:447 2C1FDB2C72AD595B1C1B5B273A02BE76ACBEF8EB cert=uD2ZRpFUB074nRFOHojpZuyN21KBxfVJKpPmcApEylPDQhRmEntoMG4mtspEGvie5kSoaA iat-mode=0

Analyze with tcpdump
--------------------

    # tcpdump -i wlan-usb-uplink -n '(not(src net 192.168.0.0/16)and(dst net 192.168.0.0/16))'
    # tcpdump -i wlan-usb-uplink -n -Q out "tcp[tcpflags] & (tcp-syn) !=0"

Backups
=======

Update backups from time to time with:

    borgmatic --verbosity 1

Installation
============

1. Image

   https://www.raspberrypi.org/software/operating-systems/

1. Installation

   https://www.raspberrypi.org/documentation/installation/installing-images/README.md

1. Headless SSH

   https://www.raspberrypi.org/documentation/remote-access/ssh/

       $ touch ssh /media/user/boot/ssh

Configuration
=============

Predictable device names
------------------------

1. Configure predictable names for each in `/etc/systemd/network/`:

   - `wlan-raspi-ap` for the internal Wi-Fi interface.

   - `eth-raspi-ap` for the internal wired interface.

   - `wlan-usb-uplink` for the USB Wi-Fi dongle.

   For example:

       [Match]
       MACAddress=ab:12:e9:ad:91:f0

       [Link]
       Name=eth-raspi-ap

Static Ethernet
---------------

1. Configure a static IP for `eth-raspi-ap` using `nmtui`.

1. Turn off wired connection on Debian in GNOME Network settings

       # ifconfig enp0s25 192.168.2.2

USB tethering
-------------

1. Plug and play :)

   Cheap USB cable might be unreliable and drop the connection all the time.

1. Fix default routes, if needed:

       # route del -net 0.0.0.0 gw 192.168.2.1
       # route del -net 0.0.0.0 gw 192.168.1.254

Wi-Fi hotspot
-------------

1. Configure a shared Wi-Fi connection ("Access Point" mode) for
   `wlan-raspi-ap` using `nmtui`.

   For example:

       [connection]
       id=turkey
       uuid=d45bb210-ae4a-4e48-bbd4-119baa9abb76
       type=wifi
       interface-name=wlan-raspi-ap
       timestamp=1736295106

       [wifi]
       mode=ap
       ssid=Wi-Fi Turkey

       [wifi-security]
       key-mgmt=wpa-psk
       psk=Turkey1234

       [ipv4]
       address1=192.168.3.1/24
       method=shared

       [ipv6]
       addr-gen-mode=default
       method=auto

       [proxy]

1. NetworkManager automatically starts dnsmasq using the configuration in
   `/etc/NetworkManager/dnsmasq-shared.d` when starting an "Access Point"
   connection.

   So, we can disable dnsmasq by default on boot:

       # systemctl disable dnsmasq.service

1. IP forwarding to usb0 in /etc/sysctl.d/routed-ap.conf:

       net.ipv4.ip_forward=1

       # iptables -t nat -A POSTROUTING -o usb0 -j MASQUERADE

1. Test SSH connection:

       ssh pi@gw.wlan

       ssh pi@gw.lan

Captive portal
--------------

https://nodogsplashdocs.readthedocs.io/en/stable/install.html#debian

1. Build the nodogsplash Debian package:

       # dpkg-buildpackage -b -rfakeroot -us -uc

1. Configure /etc/nodogsplash/nodogsplash.conf:

       GatewayInterface wlan0
       GatewayAddress 192.168.3.1

1. Disable nodogsplash by default on boot:

       # systemctl disable nodogsplash.service

1. Start nodogsplash:

       # systemctl start nodogsplash.service

Censoring websites
------------------

Spoof DNS in `/etc/NetworkManager/dnsmasq-shared.d/`.

    address=/en.wikipedia.org/192.168.3.1

Serve an error page using lighttpd.

Browsers return a security warning if the user visits the HTTP version, but
there is no way around, so it's probably a realistic behavior.
