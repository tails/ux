Your mission

You are a journalist in Mexico working on sensitive revelations about
corrupted officials.

There have been recent scandals about government spyware targeting other
journalists like you. You want to ensure the anonymity of your
journalist sources and the security of your data before you publish
about the story.

A trusted friend gives you a USB stick with Tails and recommends you to
use it for your work for more security.

Task 1

Visit the Tails website and learn a bit more about Tails:

https://tails.boum.org/

Task 2

Configure Tails to always start with a Spanish keyboard.

You are going to use Tails every day and you don't want to redo the
configuration every time.

Task 3

You received an anonymous letter with a link to a video of the State
governor receiving a bribe from the organized crime. Save it to your
Tails in a way that protects it if your USB stick is stolen.

	Wi-Fi:		Mexico Leaks

	Password: 	NpUSJBh9

	Video:		https://mexicoleaks.mx/video.mp4

Task 4

One week later, you receive a link to the same video but with more
footage and in better quality. Replace the first video with the second
video in your USB stick:

	Full video:	https://mexicoleaks.mx/video-full.mp4

Task 5

The source wants to have a voice call with you but requests to do the
call using Mumble from Tails to protect their anonymity. Save your
Mumble configuration to your USB stick.

	Mumble server: vww6ybal4bd7szmgncyruucpgfkqahzddi37ktc7ngmcopnpyyd.onion

Task 6

During one month, you will be traveling to the US and working from a
different office. The security policy of this office only allows
authorized devices to connect to the Wi-Fi, based on their MAC address.

Configure your Tails to automatically connect to this new Wi-Fi network.

	Wi-Fi:		Press Freedom

	Password:	eNQTNf1d

Task 7

After publishing the video extracts in the news, you want to delete all
the data related to this case from your Tails. You want to be able to
continue using Tails for other revelations in the future.
