The email address of the interviewees are stored in the internal Git
repo: `contacts/ux_interviews.mdwn`.

The names of the interviewees are changed but loosely related in terms
of gender, language, and age.

Asking for interviews
---------------------

Hi,

I'm sajolida and I work for Tails.

At Tails, I work on usability and part of my job is to learn about our
users. I use these stories and feedback to make Tails easier to use and
more useful for journalists and activists.

Would you be ready to talk to us about your experience?

Have a nice day,

--
sajolida

Interview script
----------------

- Introduction
  * Part of my work at Tails is to learn about users so we can make Tails
    easier to use and more useful for people like you.

- Getting the interviewee's consent:
  * You can answer my questions to the extend that you feel comfortable
    and stop at any moment.
  * We want to keep this information publicly available for contributors
    of the project but in generic terms, removing personally
    identifiable information.

- You
  * Who are you, and what do you do?

- Tails & you
  * How did you first learn about Tails and what motivated you to start using it?
  * How long have you been using Tails and how often do you use it?
  * In what situations do you typically use Tails?
  * What are the primary tasks you perform while using Tails?
  * Can you tell me about a time when using Tails was particularly important?

- Pros & cons
  * How has Tails improved your digital security or privacy?
  * What features of Tails do you find most valuable?
    - Can you tell me about a time when when these features were crucial for
      your work or personal security?
  * Have you encountered any challenges or limitations while using Tails?
    - How do you currently overcome these limitations?
    - What else do you use when you cannot use Tails?
  * What improvements or new features would you like to see in Tails?
    - Can you tell me about a time when you wished Tails had a specific feature
      or capability? How would that have helped you in that moment?

- Good bye:
  * Thank you!
  * Would you give us your email if you want to keep in touch for future
    questions or go deeper? Emails are stored encrypted and only
    accessible to the core contributors.
  * Do you know of anybody else using Tails and that would be worth
    interviewing?

Resources on interviewing users
-------------------------------

- [Steve Portigal, _Interviewing Users_](http://gen.lib.rus.ec/search.php?req=steve+portigal+interviewing+users)

- [Parlatype](https://gkarsay.github.io/parlatype/), a minimal audio
  player for manual speech transcription.

Tips when taking notes
----------------------

- Whenever possible, try to transcribe the language, mental model, and
  understanding of the interviewee. The interviewee might use a
  different word than what we usual use (for example, "permanent"
  instead of "persistent") or think that something is not possible in
  Tails while it is. Don't correct them during the interview (you can
  clarify things that could be helpful for them at the very end) and
  transcribe this in your summary.

Template email for validating the output
----------------------------------------

<pre>
Thank you so much for taking some time to answer my questions about
Tails the other day!

I'm sending you here what I plan to store in our public working
documents on our website. Please correct me if I misunderstood
something or if you want me to remove some bits.
</pre>
